# lonelyion.com

[![Hosted on Vercel](https://img.shields.io/badge/Hosted%20on%20Vercel-%24%20lonelyion%2Fhome-black?logo=Vercel&style=flat-square&labelColor=24292e)](https://vercel.com/lonelyion/home)
[![Based on Portfolio](https://img.shields.io/badge/Based%20on-spencerwooo%2Fportfolio-black?logo=github&style=flat-square&labelColor=24292e)](https://github.com/spencerwooo/portfolio)


My personal homepage.

## Project setup

```bash
#Install dependencies
yarn

#Compiles and hot-reloads for development
yarn serve

#Compiles and minifies for production
yarn build

#Lints and fixes files
yarn lint
```

## Copyright

Released under the [MIT License](./LICENSE).